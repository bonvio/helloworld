package com.springapp.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class HelloController {

	@RequestMapping(method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "" +
				//"Hello world! Если вы читаете это сообщение, то все работает правильно, <br>" +
				//"обновлено 10 февраля 2016 года, нясгр2щшвсн2нсшщл" +
				//"\n Новый коммит!!!!!!!!!!!!" +
				"\n Маша, молодец, все сделала правильно :* Hello world!");
		return "hello";
	}
	
		@RequestMapping(value = "get", method = RequestMethod.GET)
	public String printWelcome2(ModelMap model) {
		model.addAttribute("message", "Hello world! Если вы читаете это сообщение, то все работает правильно, обновлено 10 февраля 2016 года, нясгр2щшвсн2нсшщл");
		return "hello";
	}
	
}